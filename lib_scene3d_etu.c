/**
* \file		lib_scene3d_etu.c
* \brief	librairie fonctions scene 3D
* \author	Emma PARDO
* \version	1.0
* \date 	2 avril 2018
*/


#include "lib_surface.h"
#include "lib_scene3d.h"
#include "lib_objet3d.h"
#include "lib_3d.h"
#include "lib_2d.h"
#include "lib_mat.h"

/**
* \fn		t_scene3d* definirScene3d_etu(t_objet3d *pt_objet)
* \brief	Creer un Scene 3D.
* \details	Initialise une scene 3D à partir d'un objet existant (camera).
* \param	pt_objet le premier objet de la scene
* \return	Une t_scene3d
*/
t_scene3d* definirScene3d_etu(t_objet3d* pt_objet)
{
	//creation de la scene
	t_scene3d *scene = malloc(sizeof(t_scene3d));
	//ajout du premier objet a la scene
	scene->objet = pt_objet;
	//initialisation des matrices montantes et descendantes
	int i, j;
	for(i=0; i<4; i++)
	{
		for (j=0; j<4; j++)
		{
			scene->descendant[i][j] = 0;
    		scene->montant[i][j] = 0;
    		if (i==j)
    		{
    			scene->descendant[i][j] = 1;
    			scene->montant[i][j] = 1;
    		}
		}
	}
	//init des autres attributs
    scene->pt_pere = NULL; 
    scene->pt_fils = NULL; 
    scene->pt_suiv = NULL;
	return scene;
};

// ajout de l'objet en tete des fils
// ajout d'objet a la scene
/**
* \fn		t_scene3d* definirScene3d_etu(t_objet3d *pt_objet)
* \brief	Rajoute un objet 3D dans la scène.
* \details	Rajoute un objet 3D sur un noeud la scène.
* \param    pt_feuille le noeud où l'on veut rajouter l'objet
* \param	pt_objet l'objet à rajouter
* \return	Le noeud contenant l'objet
*/
t_scene3d* ajouter_relation_etu(t_scene3d *pt_feuille, t_objet3d *pt_objet)
{
	//creation d'une scene file avec l'objet à rajouter
	t_scene3d *sceneFille = definirScene3d_etu(pt_objet);
	//le père de la scene fille est la scene courante (feuille)
	sceneFille->pt_pere = pt_feuille;
	//le fils de la scene fille est le fils de la scene courante
	sceneFille->pt_suiv = pt_feuille->pt_fils;
	//la fille de la scene courante est la scene fille
	pt_feuille->pt_fils = sceneFille;
	//on retourne la scene courante
	return sceneFille;
};

/**
* \fn		void translationScene3d_etu(t_scene3d *pt_scene, t_point3d *vecteur)
* \brief	Change la position de la scene dans le repère
* \details	
* \param    pt_scene la scene à modifier
* \param	vecteur le vecteur de translation
* \return	void
*/
void translationScene3d_etu(t_scene3d *pt_scene, t_point3d *vecteur)
{
	int i,j; 
	//si la scene n'est pas nulle
	if (pt_scene != NULL) 
	{
      	//creation de matrices
      	double des[4][4];
       	double mon[4][4];
       	double tp[4][4];

       	//si le vecteur n'est pas nul
		if(vecteur != NULL) 
		{
			//init des matrices
			for(i=0; i<4; i++)
			{
				for (j=0; j<4; j++)
				{
					des[i][j] = 0;
    				mon[i][j] = 0;
    				if (i==j)
    				{
    					des[i][j] = 1;
    					mon[i][j] = 1;
    				}
				}
			}
			//si l'objet est une caméra
			if (pt_scene->objet->est_camera)
			{
				//translation a droite de la matrice montante
	            mon[0][3] = vecteur->xyzt[0];
	            mon[1][3] = vecteur->xyzt[1];
	            mon[2][3] = vecteur->xyzt[2];

	            //translation inverse à droite de la matrice descendante
	            des[0][3] = -vecteur->xyzt[0];
	            des[1][3] = -vecteur->xyzt[1];
	            des[2][3] = -vecteur->xyzt[2];
        	}
	        else
	        {
	        	//translation a droite de la matrice montante
		        mon[0][3] = -vecteur->xyzt[0];
		        mon[1][3] = -vecteur->xyzt[1];
		        mon[2][3] = -vecteur->xyzt[2]; 
	          	//translation inverse à droite de la matrice descendante
	          	des[0][3] = vecteur->xyzt[0];
	          	des[1][3] = vecteur->xyzt[1];
	           	des[2][3] = vecteur->xyzt[2];
        	}
    	}

		//multiplication des matrices de translation
		multiplication_matrice(tp, des, pt_scene->descendant);
		for(i = 0;i<4;i++)
		{
			for(j=0;j<4;j++)
				{ pt_scene->descendant[i][j] = tp[i][j]; }
		}

		multiplication_matrice(tp, mon, pt_scene->montant);
		for(i = 0;i<4;i++)
		{
  			for(j=0;j<4;j++)
				{ pt_scene->montant[i][j] = tp[i][j]; }
		}
	}
};

/**
* \fn		void rotationScene3d_etu(t_scene3d *pt_scene, t_point3d *centre, float degreX, float degreY, float degreZ)
* \brief	Fait pivoter la scene.
* \details	On fait appelle a rotationObjet3d_etu() pour pivoter tous les objets de la scene
* \param    pt_scene la premier scene à pivoter
* \param 	centre de l'objet representant le Point de rotation
* \param	degreX un reel representant l angle de rotation en X
* \param	degreY un reel representant l angle de rotation en Y
* \param	degreZ un reel representant l angle de rotation en Z
* \return	void
*/
void rotationScene3d_etu(t_scene3d *pt_scene, t_point3d *centre, float degreX, float degreY, float degreZ)
{
	/*
	//on parcourt les scenes
	while (pt_scene!=NULL)
	{
		//on applique la rotation sur chaque objet de la scene
		rotationObjet3d_etu(pt_scene->objet, centre, degreX, degreY, degreZ);
		//on passe a la scene suivante
		pt_scene = pt_scene->pt_suiv;
	}*/
};

/**
* \fn		void dessinerScene3d_etu(t_surface *surface, t_scene3d* pt_scene)
* \brief	Colorie une scene3d
* \details	On fait appel a remplirTriangle3d_etu() pour traiter chaque face.
* \param	surface une surface
* \param	pt_scene la scene à colorier
* \return	void
*/
void dessinerScene3d_etu(t_surface *surface, t_scene3d* pt_scene)
{
	/*
	//on sauvegarde le premier objet qui est la caméra
	t_objet3d *cam = pt_scene->objet;
	//on parcourt les scenes
	while (pt_scene!=NULL)
	{
		//on récupère le second objet de la scene
		pt_scene = pt_scene->pt_fils;
		//on dessine l'objet de la scene
		dessinerObjet3d_etu(surface, pt_scene->objet, cam);
		//on passe a la scene suivante
		pt_scene = pt_scene->pt_suiv;
	}*/
};

/**
* \fn		void changerCamera_etu(t_scene3d *pt_objet)
* \brief	Change la caméra actuelle de la scene.
* \details	Modifie l'arbre de la scene pour que pt_objet en soit la racine
* \param	pt_objet la nouvelle caméra
* \return	void
*/
void changerCamera_etu(t_scene3d *pt_objet)
{
	t_scene3d *scene = malloc(sizeof(t_scene3d));
	scene = pt_objet;
    //on parcourt les scenes jusqu'a atteindre la premiere scene
	while (scene->pt_pere!=NULL)
	{	//on passe a la scene suivante
		scene = pt_objet->pt_pere;
	}
	if(scene->pt_pere == NULL)
	{
		// on rajoute l'objet tout en haut de l'arbre (noeud sans père)
		scene = pt_objet;
	}
};
