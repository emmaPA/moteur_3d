/**
* \file		lib_3d_etu.c
* \brief	librairie fonctions 3D
* \author	Emma PARDO
* \version	1.0
* \date 	22 fevrier 2018

*iuoyi
*/

#include "lib_surface.h"
#include "lib_3d.h"
#include "lib_2d.h"
#include "lib_mat.h"
#include <math.h>

/**
* \fn		t_point3d *definirPoint3d_etu(double x, double y, double z)
* \brief	Creer un point 3D.
* \details	Le point est compose de 4 coordonnees. La derniere etant une coordonnee homogene egale a 1.
* \param	x 	coordonnee en x
* \param	y 	coordonnee en z
* \param	z 	coordonnee en y
* \return	Un point3D avec les coordonnees passees en parametres.
*/

t_point3d *definirPoint3d_etu(double x, double y, double z)	// attention malloc
{
	t_point3d *p;

	p  = (t_point3d*) malloc(sizeof(t_point3d));
	if (p!=NULL)
	{
		p->xyzt[0] = x;
		p->xyzt[1] = y;
		p->xyzt[2] = z;
		p->xyzt[3] = 1;
	}

	return p;
}

/**
* \fn		t_point3d *definirVecteur3d_etu(double x, double y, double z)
* \brief	Creer un vecteur 3D.
* \details	Le vecteur est compose de 4 coordonnees. La derniere etant une coordonnee homogene egale a 0.
* \param	x 	coordonnee en x
* \param	y 	coordonnee en z
* \param	z 	coordonnee en y
* \return	Un point3D avec les coordonnees passees en parametres.
*/

t_point3d *definirVecteur3d_etu(double x, double y, double z)
{
	t_point3d *p;

	p  = (t_point3d*) malloc(sizeof(t_point3d));
	if (p!=NULL)
	{
		p->xyzt[0] = x;
		p->xyzt[1] = y;
		p->xyzt[2] = z;
		p->xyzt[3] = 0;
	}

	return p;
}

/**
* \fn		t_point3d *copierPoint3d_etu(t_point3d *p)
* \brief	Copier un Point en 3D.
* \details	On cree un nouveau Point a partir d un Point existant, pour cela on fait appel a la fonction de creation de t_point3d.
* \param	p 	un t_point3d
* \return	Un t_point3d
*/

t_point3d *copierPoint3d_etu(t_point3d *p)
{
	t_point3d *new_p;

	new_p = definirPoint3d_etu(p->xyzt[0], p->xyzt[1], p->xyzt[2]);

	return new_p;
}

/**
* \fn		t_triangle3d *definirTriangle3d_etu(t_point3d * a, t_point3d * b, t_point3d * c)
* \brief	Creer un Triangle en 3D.
* \details	Le Triangle est compose de 3 sommets, les pointeurs sur t_point3d. 
* \param	a 	un t_point3d
* \param	b 	un t_point3d
* \param	c 	un t_point3d
* \return	Un t_triangle3d
*/

t_triangle3d *definirTriangle3d_etu(t_point3d * a, t_point3d * b, t_point3d * c)
{
	t_triangle3d *t;
	t  = (t_triangle3d*) malloc(sizeof(t_triangle3d));
	if (t!=NULL)
	{
		t->abc[0] = a;
		t->abc[1] = b;
		t->abc[2] = c;
	}
	return t;
}

/**
* \fn		t_triangle3d *copierTriangle3d_etu(t_triangle3d *t)
* \brief	Copier un Triangle en 3D.
* \details	On cree un nouveau triangle a partir d un triangle existant, pour cela on fait appel aux fonctions de creation de t_point3d et de t_triangle3d. 
* \param	t 	un t_triangle3d
* \return	Un t_triangle3d
*/

t_triangle3d *copierTriangle3d_etu(t_triangle3d *t)
{
	t_triangle3d *new_t;

	new_t = definirTriangle3d_etu(definirPoint3d_etu(t->abc[0]->xyzt[0], t->abc[0]->xyzt[1],t->abc[0]->xyzt[2]),
			definirPoint3d_etu(t->abc[1]->xyzt[0], t->abc[1]->xyzt[1],t->abc[1]->xyzt[2]),
			definirPoint3d_etu(t->abc[2]->xyzt[0], t->abc[2]->xyzt[1],t->abc[2]->xyzt[2]));

	return new_t;
}

/**
* \fn		void differenceVecteur3d_etu(t_point3d *v1, t_point3d *v2)
* \brief	Calculer la difference entre deux vecteurs 3D en coordonnees homogenes.
* \details	vx = vx - vy
* \param	v1 	1er vecteur
* \param	v2 	2eme vecteur
* \return	void
*/

void differenceVecteur3d_etu(t_point3d *v1, t_point3d *v2)
{
	v1->xyzt[0] = v1->xyzt[0] - v2->xyzt[0];
	v1->xyzt[1] = v1->xyzt[1] - v2->xyzt[1];
	v1->xyzt[2] = v1->xyzt[2] - v2->xyzt[2];
	v1->xyzt[3] = 0;
} 

/**
* \fn		void sommeVecteur3d_etu(t_point3d *v1, t_point3d *v2)
* \brief	Calculer la somme entre deux vecteurs 3D en coordonnees homogenes.
* \details	vx = vx + vy
* \param	v1 	1er vecteur
* \param	v2 	2eme vecteur
* \return	void
*/

void sommeVecteur3d_etu(t_point3d *v1, t_point3d *v2)
{
	v1->xyzt[0] = v1->xyzt[0] + v2->xyzt[0];
	v1->xyzt[1] = v1->xyzt[1] + v2->xyzt[1];
	v1->xyzt[2] = v1->xyzt[2] + v2->xyzt[2];
	v1->xyzt[3] = 0;
}

/**
* \fn		void divisionVecteur3d_etu(t_point3d *v1, int n)
* \brief	Calculer la quotien entre un vecteurs 3D et une norme. Cette fonction permet de normaliser le vecteur.
* \details	vx = vx / n
* \param	v1 	1er vecteur
* \param	n 	une norme (mathématiques)
* \return	void
*/

void divisionVecteur3d_etu(t_point3d *v1, int n)
{
	v1->xyzt[0] = v1->xyzt[0] / n;
	v1->xyzt[1] = v1->xyzt[1] / n;
	v1->xyzt[2] = v1->xyzt[2] / n;
	v1->xyzt[3] = 0;
}

/**
* \fn		t_point3d* centreGraviteTriangle3d_etu(t_triangle3d *t)
* \brief	Calculer le centre de gravite d un Triangle 3D.
* \details	Pour trouver le Point A correspondant on utilise les coordonnees cartesiennes du Triangle de sommets B,C et D.
* 			A[X]= (B[X] + C[X] + D[X]) /3 pour X de 0 a 2.
* \param	t un t_point3d
* \return	un t_point3d
*/
t_point3d* centreGraviteTriangle3d_etu(t_triangle3d *t)
{
	t_point3d *p_grv;

	p_grv = definirPoint3d_etu((t->abc[0]->xyzt[0] + t->abc[1]->xyzt[0] + t->abc[2]->xyzt[0])/3,
							   (t->abc[0]->xyzt[1] + t->abc[1]->xyzt[1] + t->abc[2]->xyzt[1])/3,
							   (t->abc[0]->xyzt[2] + t->abc[1]->xyzt[2] + t->abc[2]->xyzt[2])/3);

	return p_grv;
}

/**
* \fn		double zmoyen_etu(t_triangle3d *t)
* \brief	Calculer la coordonnee z moyenne d un Triangle 3D.
* \details	Zmoy = (Z1 + Z2 + Z3)/3
* \param	t un t_triangle3d
* \return	un nombre double
*/

double zmoyen_etu(t_triangle3d *t)
{
	double Z_moy;
	Z_moy = (t->abc[0]->xyzt[2] + t->abc[1]->xyzt[2] + t->abc[2]->xyzt[2])/3;
	return Z_moy;
}

/**
* \fn		void remplirTriangle3d_etu(t_surface * surface, t_triangle3d * triangle, Uint32 c, double l, double h, double d)
* \brief	Remplir la surface d un Triangle 3D.
* \details	
* \param	surface une surface
* \param	triangle un t_triangle3d
* \param	c un entier representant la couleur
* \param	l un reel representant la longeur de la camera
* \param	h un reel representant la hauteur de la camera
* \param	d un reel representant la distance focale
* \return	void
*/

void remplirTriangle3d_etu(t_surface * surface, t_triangle3d * triangle, Uint32 c, double l, double h, double d)
{
	int i,j;

	t_point2d *m = (t_point2d *) malloc(sizeof(t_point2d));
	t_point2d *n = (t_point2d *) malloc(sizeof(t_point2d));
	t_point2d *o = (t_point2d *) malloc(sizeof(t_point2d));
	t_triangle2d *t2d = (t_triangle2d *) malloc(sizeof(t_triangle2d));

	//projection perspective (3d->2D)
	m->x = (l/2) + d * (triangle->abc[0]->xyzt[0])/(triangle->abc[0]->xyzt[2]);
	m->y = (h/2) + d * (triangle->abc[0]->xyzt[1])/(triangle->abc[0]->xyzt[2]);
	n->x = (l/2) + d * (triangle->abc[1]->xyzt[0])/(triangle->abc[1]->xyzt[2]);
	n->y = (h/2) + d * (triangle->abc[1]->xyzt[1])/(triangle->abc[1]->xyzt[2]);
	o->x = (l/2) + d * (triangle->abc[2]->xyzt[0])/(triangle->abc[2]->xyzt[2]);
	o->y = (h/2) + d * (triangle->abc[2]->xyzt[1])/(triangle->abc[2]->xyzt[2]);
	
	// Definition du nouveau triangle 2D grace aux points reccuperes
	t2d = definirTriangle2d(m,n,o);

	//Remplissage du triangle 2D
	remplirTriangle2d(surface,t2d,c);

	// Liberation de l'espace memoire
	free(m);
	free(n);
	free(o);
	free(t2d);
};

/**
* \fn		void translationTriangle3d_etu(t_triangle3d *t, t_point3d *vecteur)
* \brief	Appliquer une translation a un Triangle 3D.
* \details	On ajoute a chaque coordonnees du Triangle celle du vecteur.
* \param	t un t_triangle3d
* \param	vecteur un t_point3d
* \return	void
*/

void translationTriangle3d_etu(t_triangle3d *t, t_point3d *vecteur)
{
	//definition de la matrice de translation
	double m_translation[4][4];
	int i, j, k;
	for (i=0; i<4; i++)
	{
		for (j=0; j<4; j++)
		{
			if (i==j)
			{
				m_translation[i][j] = 1;
			}
			else if (j==3)
			{
				m_translation[i][j] = vecteur->xyzt[i];
			}
			else 
			{
				m_translation[i][j] = 0;
			}
		}
	}
	//calcul de la translation
	for (k=0; k<3; k++)
	{
		multiplication_vecteur(t->abc[k], m_translation, vecteur);
	}
}
/**
* \fn		void rotationTriangle3d_etu(t_triangle3d *t, t_point3d *centre, float degreX, float degreY, float degreZ)
* \brief	Appliquer une rotation a un Triangle 3D.
* \details	La matrice de rotation utilisee exprime la rotation du Triangle autour de n importe quel axe.
			On deplace l'objet a l origine du repere avant d effectuer la rotation puis on le remet a sa place (point centre).
* \param	t un t_triangle3d
* \param 	centre un t_point3d representant le Point de rotation
* \param	degreX un reel representant l angle de rotation en X
* \param	degreY un reel representant l angle de rotation en Y
* \param	degreZ un reel representant l angle de rotation en Z
* \return	void
*/

void rotationTriangle3d_etu(t_triangle3d *t, t_point3d *centre, float degreX, float degreY, float degreZ)
{
	// on place l'objet au centre du repere
	t_point3d *centre_origine;
	t_point3d *origine_centre;
	origine_centre = definirVecteur3d_etu(centre->xyzt[0], centre->xyzt[1], centre->xyzt[2]);
	centre_origine = definirVecteur3d_etu(-centre->xyzt[0], -centre->xyzt[1], -centre->xyzt[2]);
	translationTriangle3d_etu(t, centre_origine);

	degreX = degreX/180*M_PI;
	degreY = degreY/180*M_PI;
	degreZ = degreZ/180*M_PI;

	//initialisations des matrices de rotation
	double m_rotationX[4][4];
	double m_rotationY[4][4];
	double m_rotationZ[4][4];
	int i, j, k;
	for (i=0; i<4; i++)
	{
		for (j=0; j<3; j++)
		{
			if (i==j)
			{
				m_rotationX[i][j] = 1;
				m_rotationY[i][j] = 1;
				m_rotationZ[i][j] = 1;
			}
			else
			{
				m_rotationX[i][j] = 0;
				m_rotationY[i][j] = 0;
				m_rotationZ[i][j] = 0;
			}
		}
	}
	// precision de la matrice de rotation de l axe X
	m_rotationX[0][0] = 1;
	m_rotationX[1][1] = cos(degreX);
	m_rotationX[1][2] = -sin(degreX);
	m_rotationX[2][1] = sin(degreX);
	m_rotationX[2][2] = cos(degreX);

	// precision de la matrice de rotation de l axe Y
	m_rotationY[0][0] = cos(degreY);
	m_rotationY[0][2] = sin(degreY);
	m_rotationY[1][1] = 1;
	m_rotationY[2][0] = -sin(degreY);
	m_rotationY[2][2] = cos(degreY);

	// precision de la matrice de rotation de l axe Z
	m_rotationZ[0][0] = cos(degreZ);
	m_rotationZ[0][1] = -sin(degreZ);
	m_rotationZ[1][0] = sin(degreZ);
	m_rotationZ[1][1] = cos(degreZ);
	m_rotationZ[2][2] = 1;

	// on effectue la rotation en multipliant les matrices
	for (k=0; k<3; k++)
	{
		multiplication_vecteur(t->abc[k], m_rotationX, t->abc[k]);
	}
	for (k=0; k<3; k++)
	{
		multiplication_vecteur(t->abc[k], m_rotationY, t->abc[k]);
	}
	for (k=0; k<3; k++)
	{
		multiplication_vecteur(t->abc[k], m_rotationZ, t->abc[k]);
	}

	// on replace le triangle
	translationTriangle3d_etu(t, origine_centre);
}



void __copierPoint3d_etu(t_point3d * src, t_point3d * dest)
{
	
	memcpy(dest->xyzt, src->xyzt, 4*sizeof(double));
}



void transformationTriangle3d_etu(t_triangle3d *t, double mat[4][4])
{
	t_point3d *p3dtmp;
	int i;

	p3dtmp = (t_point3d*)malloc(sizeof(t_point3d));
	if (p3dtmp!=NULL)
	{
		for(i=0; i<3; i++)
		{
			multiplication_vecteur(p3dtmp, mat, t->abc[i]);
			__copierPoint3d_etu(p3dtmp, t->abc[i]);

		}
	}

	free(p3dtmp);
}