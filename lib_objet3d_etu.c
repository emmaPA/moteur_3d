/**
* \file		lib_objet3d_etu.c
* \brief	librairie fonctions objets 3D
* \author	Emma PARDO
* \version	1.0
* \date 	22 fevrier 2018
*/

#include "lib_surface.h"
#include "lib_objet3d.h"
#include "lib_3d.h"
#include "lib_2d.h"
#include "lib_mat.h"
#include "math.h"

/**
* \fn		t_objet3d* objet_vide_etu()
* \brief	Creer un Objet en 3D.
* \details	L Objet est cree mais vide.
* \param	void
* \return	Un t_objet3d
*/

t_objet3d* objet_vide_etu()
{
	t_objet3d *o;
	o = (t_objet3d*) malloc(sizeof(t_objet3d));

	o->est_trie = false;
	o->est_camera = false;
	o->tete = NULL;
	return o;
}

/**
* \fn		t_objet3d *camera_etu(double l, double h, double n, double f, double d)
* \brief	Creer un Objet 3D Camera.
* \details	L Objet est cree et initialise pour etre une camera. // zone l*h dans le champ n->f
* \param	l la largeur de l ouverture de la camera
* \param	h la hauteur de l ouverture de la camera
* \param	n la distance proche de l espace 3D
* \param	f la distance lointaine de l espace 3D
* \param	d la distance focale de la camera
* \return	Un t_objet3d (camera)
*/

t_objet3d *camera_etu(double l, double h, double n, double f, double d)
{
	t_objet3d *o_cam;
	
	o_cam = (t_objet3d*) malloc(sizeof(t_objet3d));

	o_cam->est_trie = false;
	o_cam->est_camera = true;
	o_cam->largeur = l;
	o_cam->hauteur = h;
	o_cam->proche = n;
	o_cam->loin = f;
	o_cam->distance_ecran = d; 
    o_cam->tete = NULL;
	
	return o_cam;
}

/**
* \fn		t_objet3d* parallelepipede_etu(double lx, double ly, double lz)
* \brief	Creer un Objet 3D Parallelepipede.
* \details	L Objet est cree et initialise pour etre un Parallelepipede. Il est compose de 6 faces carrees soit 12 faces triangulaires.
* 			On place l objet par defaut au centre du repere, donc les longueurs lx, ly et lz sont divisees par 2.
* \param	lx la longeur sur l axe x
* \param	ly la longeur sur l axe y
* \param	lz la longeur sur l axe z
* \return	Un t_objet3d (Parallelepipede)
*/

t_objet3d* parallelepipede_etu(double lx, double ly, double lz)
{
	t_objet3d *para = objet_vide_etu();

	//creation des 8 points
	t_point3d *A = definirPoint3d_etu(-lx/2, -ly/2, -lz/2);
	t_point3d *B = definirPoint3d_etu( lx/2, -ly/2, -lz/2);
	t_point3d *C = definirPoint3d_etu( lx/2,  ly/2, -lz/2);
	t_point3d *D = definirPoint3d_etu(-lx/2,  ly/2, -lz/2);

	t_point3d *E = definirPoint3d_etu(-lx/2, -ly/2,  lz/2);
	t_point3d *F = definirPoint3d_etu( lx/2, -ly/2,  lz/2);
	t_point3d *G = definirPoint3d_etu( lx/2,  ly/2,  lz/2);
	t_point3d *H = definirPoint3d_etu(-lx/2,  ly/2,  lz/2);
	
	//creation des 12 faces
	int cpt;
	t_maillon *mp;
	mp = (t_maillon*) malloc(sizeof(t_maillon));

	for (cpt=0; cpt<12; cpt++)
	{
		t_maillon *m;
		m = (t_maillon*) malloc(sizeof(t_maillon));
		m->pt_suiv = NULL;
		switch(cpt)
		{
			case 0: //face 1 ABFE
				m->face = definirTriangle3d_etu(B, A, E);
				m->couleur = GRISF;
				para->tete = m;
			break;
			case 1:
				m->face = definirTriangle3d_etu(B, F, E);
				m->couleur = BLANC;
			break;
			case 2: //face 2 BCGF
				m->face = definirTriangle3d_etu(C, B, F);
				m->couleur = ROUGEC;
			break;
			case 3:
				m->face = definirTriangle3d_etu(C, G, F);
				m->couleur = ROUGEF;
			break;
			case 4: //face 3 ADHE
				m->face = definirTriangle3d_etu(A, D, H);
				m->couleur = VERTC;
			break;
			case 5:
				m->face = definirTriangle3d_etu(A, E, H);
				m->couleur = VERTF;
				mp->pt_suiv = m;
			break;
			case 6: //face 4 CDHG
				m->face = definirTriangle3d_etu(D, C, G);
				m->couleur = BLEUC;
			break;
			case 7:
				m->face = definirTriangle3d_etu(D, H, G);
				m->couleur = BLEUF;
			break;
			case 8: //face 5 ABCD
				m->face = definirTriangle3d_etu(B, A, D);
				m->couleur = JAUNEC;
			break;
			case 9:
				m->face = definirTriangle3d_etu(B, C, D);
				m->couleur = JAUNEF;
			break;
			case 10: //face 6 EFGH
				m->face = definirTriangle3d_etu(E, F, G);
				m->couleur = ROSEC;
			break;
			case 11:
				m->face = definirTriangle3d_etu(E, H, G);
				m->couleur = ROSEF;
			break;
		}
		mp->pt_suiv = m;
		mp = m;	
	}
	return para;
};

/**
* \fn		t_objet3d* sphere_etu(double r, double nlat, double nlong)
* \brief	Creer un Objet 3D Sphere.
* \details	L Objet est cree et initialise pour etre une Sphere.

* \param	r le rayon de la Sphere
* \param	nlat le nombre de latitude de la Sphere
* \param	nlong le nombre de longitude de la Sphere
* \return	Un t_objet3d (Sphere)
*/

t_objet3d* sphere_etu(double r, double nlat, double nlong)
{
	t_objet3d *sphere = objet_vide();
	double theta, phi, x, y, z;
	t_maillon *mp = (t_maillon*) malloc(sizeof(t_maillon));

    //tableau de stockage des points
    t_point3d *tabpoints[(int) nlat][(int) nlong];

    //Définition des points des faces de la sphère
    for (int i = 1; i < nlat + 1; ++i)
	{
        theta = (i * M_PI) / (nlat + 1);
        for (int j = 0; j < nlong; ++j)
		{
            phi = (j * 2 * M_PI) / nlong;

            x = r * sin(theta) * sin(phi);
            y = -r * cos(theta);
            z = r * sin(theta) * cos(phi);

            tabpoints[i -1][j] = definirPoint3d(x, y, z);
        }
    }

    //création des faces
    for(int i = 0; i < nlat -1; ++i)
	{
        for(int j = 0; j < nlong -1; ++j)
		{
			//création des maillons
           	t_maillon *maillon1 = malloc(sizeof(t_maillon));
           	t_maillon *maillon2 = malloc(sizeof(t_maillon));

           	maillon1->face = definirTriangle3d(tabpoints[i][j], tabpoints[i][j + 1], tabpoints[i + 1][j + 1]);
           	maillon2->face = definirTriangle3d(tabpoints[i][j], tabpoints[i + 1][j], tabpoints[i + 1][j + 1]);
			
			//couleur
			if((i+j)%2 == 0)
			{
				maillon1->couleur = VERTC;
				maillon2->couleur = BLEUC;
			}
			else
			{
				maillon1->couleur = ROUGEC;
				maillon2->couleur = JAUNEC;
			}

           	maillon1->pt_suiv = maillon2;

			//On parcourt
            if(i == 0 && j == 0)
				{ sphere->tete = maillon1; }
			else
				{ mp->pt_suiv = maillon1; }
           	mp = maillon2;
        }
    }
    return sphere;
};

/**
* \fn		t_objet3d* sphere_amiga_etu(double r, double nlat, double nlong)
* \brief	Creer un Objet 3D Sphere Amiga.
* \details	L Objet est cree et initialise pour etre une Sphere Amiga. Les faces sont alternativement rouges et blanches.

* \param	r le rayon de la Sphere
* \param	nlat la latitude de la Sphere
* \param	nlong la longitude de la Sphere
* \return	Un t_objet3d (Sphere)
*/

t_objet3d* sphere_amiga_etu(double r, double nlat, double nlong)
{
	t_objet3d *sphere = objet_vide();
	double theta, phi, x, y, z;
	t_maillon *mp = (t_maillon*) malloc(sizeof(t_maillon));

    //tableau de stockage des points
    t_point3d *tabpoints[(int) nlat][(int) nlong];

    //Définition des points des faces de la sphère
    for (int i = 1; i < nlat + 1; ++i)
	{
        theta = (i * M_PI) / (nlat + 1);
        for (int j = 0; j < nlong; ++j)
		{
            phi = (j * 2 * M_PI) / nlong;

            x = r * sin(theta) * sin(phi);
            y = -r * cos(theta);
            z = r * sin(theta) * cos(phi);

            tabpoints[i -1][j] = definirPoint3d(x, y, z);
        }
    }

    //création des faces
    for(int i = 0; i < nlat -1; ++i)
	{
        for(int j = 0; j < nlong -1; ++j)
		{
			//création des maillons
           	t_maillon *maillon1 = malloc(sizeof(t_maillon));
           	t_maillon *maillon2 = malloc(sizeof(t_maillon));

           	maillon1->face = definirTriangle3d(tabpoints[i][j], tabpoints[i][j + 1], tabpoints[i + 1][j + 1]);
           	maillon2->face = definirTriangle3d(tabpoints[i][j], tabpoints[i + 1][j], tabpoints[i + 1][j + 1]);
			
			//couleur
			if((i+j)%2 == 0)
			{
				maillon1->couleur = ROUGEC;
				maillon2->couleur = BLANC;
			}
			else
			{
				maillon1->couleur = ROUGEC;
				maillon2->couleur = BLANC;
			}

           	maillon1->pt_suiv = maillon2;

			//On parcourt
            if(i == 0 && j == 0)
				{ sphere->tete = maillon1; }
			else
				{ mp->pt_suiv = maillon1; }
           	mp = maillon2;
        }
    }
    return sphere;
};

/**
* \fn		t_objet3d* arbre_etu(double lx, double ly, double lz)
* \brief	Creer un Objet 3D Arbre.
* \details	L Objet est cree et initialise pour etre un Arbre. L Arbre est compose d un parallepipede et
			d une pyramide.
* \param	lx la largeur
* \param	ly la hauteur
* \param	lz la longeur
* \return	Un t_objet3d (Arbre)
*/

t_objet3d* arbre_etu(double lx, double ly, double lz)
{
	t_objet3d *arbre = objet_vide_etu();
	t_objet3d *tronc = parallelepipede_etu(lx, ly, lz);

	//création d'un maillon temporaire qui fera le lien entre les maillons du parallépipède
	//et de la pyramide.
	t_maillon *mp = (t_maillon*) malloc(sizeof(t_maillon));
	mp = tronc->tete;
	while(mp != NULL)
	{
		mp->couleur = MARRON1;
		mp = mp->pt_suiv;
	}

	//création de la pyramide
	t_point3d *C = definirPoint3d_etu( lx/2,  ly/2, -lz/2);
	t_point3d *D = definirPoint3d_etu(-lx/2,  ly/2, -lz/2);
	t_point3d *G = definirPoint3d_etu( lx/2,  ly/2,  lz/2);
	t_point3d *H = definirPoint3d_etu(-lx/2,  ly/2,  lz/2);
	t_point3d *S = definirPoint3d_etu(    0,    ly,     0);

	int cpt;
	for (cpt = 0; cpt<4; cpt++)
	{
		t_maillon *m = (t_maillon*) malloc(sizeof(t_maillon));
		m->pt_suiv = NULL;
		switch(cpt)
		{
			case 0:
				m->face = definirTriangle3d_etu(D, S, H);
				arbre->tete = m;
			break;
			case 1:
				m->face = definirTriangle3d_etu(H, S, G);
				mp->pt_suiv = m;
			break;
			case 2:
				m->face = definirTriangle3d_etu(G, S, C);
				mp->pt_suiv = m;
			break;
			case 3:
				m->face = definirTriangle3d_etu(C, S, D);
				mp->pt_suiv = m;
			break;
		}
		m->couleur = VERTC;
		mp = m;
		//on lie les deux objets: le dernier maillon du parallépipède est le premier de la pyramide
		mp->pt_suiv = tronc->tete;
	}
	return arbre;
};

/**
* \fn		t_objet3d* damier_etu(double lx, double lz, double nx, double nz)
* \brief	Créer un Objet3D damier
* \details	
* \param	lx la longueur
* \param	lz la largeur
* \param	nx nb de carrées en longueur
* \param	nz nb de carrées en largeur
* \return	Un t_objet3d (damier)
*/
t_objet3d* damier_etu(double lx, double lz, double nx, double nz)
{
	t_objet3d *damier= objet_vide_etu();
	t_maillon *mp = (t_maillon*) malloc(sizeof(t_maillon));
	t_bool btete = true;
	int i,j;

	//creation des dalles
	for(i=0; i<nz; i++)
	{
		for(j=0; j<nx; j++)
		{
			//definition des points
			double x = (i * lx) / nx - lx / 2;
			double z =  (j * lz) / nz - lz / 2;
			double xx = ((i + 1) * lx) / nx - lx / 2;
			double zz = ((j + 1) * lz) / nz - lz / 2;

			t_point3d *A = definirPoint3d_etu( x,  0, z );
			t_point3d *B = definirPoint3d_etu( x,  0, zz);
			t_point3d *C = definirPoint3d_etu( xx, 0, z );
			t_point3d *D = definirPoint3d_etu( xx, 0, zz);

			//init des maillons/faces
			t_maillon *m1 = (t_maillon*) malloc(sizeof(t_maillon));
			t_maillon *m2 = (t_maillon*) malloc(sizeof(t_maillon));

			m1->pt_suiv = m2;

			//tracage de la face
			m1->face = definirTriangle3d_etu(A, C, D);
			m2->face = definirTriangle3d_etu(A, B, D);

			//gestion des couleurs
			if(i%2 == 0)
			{
				if(j%2 == 0)
				{
					m1->couleur = BLANC;
					m2->couleur = BLANC;
				}
				else
				{
					m1->couleur = NOIR;
					m2->couleur = NOIR;
				}
			}
			else
			{
				if(j%2 == 0)
				{
					m1->couleur = NOIR;
					m2->couleur = NOIR;
				}
				else
				{
					m1->couleur = BLANC;
					m2->couleur = BLANC;
				}
			}
			
			if(btete == true)
			{
				damier->tete = m1;
				btete = false;
			}
			else 
				{ mp->pt_suiv = m1; }
			
			mp = m2;
		}
	}
	return damier;
};



/**
* \fn		t_maillon * copierMaillon(t_maillon *m)
* \brief	fonction annexe copierObjet3d, elle permet de copier les maillons d'un objet de manière récursive
* \details	
* \param	m le maillon à copier
* \return	un t_maillon
*/
t_maillon * copierMaillon(t_maillon *m)
{
	if(m == NULL)
		{ return NULL; }
	//nouveau maillon copié
	t_maillon *maillon = malloc(sizeof(t_maillon));
	maillon->couleur = m->couleur;
	maillon->face = copierTriangle3d(m->face);
	maillon->pt_suiv = copierMaillon(m->pt_suiv);
	return maillon;
}

/**
* \fn		t_objet3d *copierObjet3d_etu(t_objet3d *o)
* \brief	On effectue une copie mirroir de l Objet 3D
* \details	
* \param	o l objet a supprimer
* \return	un t_objet3d
*/
// ne fonctionne pas -> coreDumped
t_objet3d *copierObjet3d_etu(t_objet3d *o)
{
	t_objet3d * new;
	//copie d'une camera
	if(o->est_camera)
		{ new = camera_etu(o->largeur, o->hauteur, o->proche, o->loin, o->distance_ecran); }
	//copie d'un objet
	else
		{ new = objet_vide_etu(); }

	//si l'objet n'est pas nul
	if(o->tete != NULL)
		{ new->tete = copierMaillon(o->tete); }
	else
	{ new->tete = NULL;}
	return new;
}

/**
* \fn		void composerObjet3d_etu(t_objet3d* o, t_objet3d* o2)
* \brief	On compose un objet à un deuxième objet
* \details	Le second objet ajouté au premier objet ne sera plus utilisable. L'objet composé prend la place du premier objet.
* \param	o un objet
* \param	o un second objet
* \return	void
*/
void composerObjet3d_etu(t_objet3d* o, t_objet3d* o2)
{
	//si l'objet n'est pas nul)
	if(o->tete != NULL)
	{
		//copie de la tete de l'objet
		t_maillon *mp = o->tete;

		//on parcourt les maillons jusqu'au dernier
		while(mp->pt_suiv != NULL)
			{ mp = mp->pt_suiv; }

		//au dernier maillon on le lie au maillon tete du second objet
		mp->pt_suiv = o2->tete;
	}
	//si l'objet est nul
	else
		{ o->tete = o2->tete; }
	//on libere la mémoire du second objet
	free(o2);
};

/**
* \fn		void composerObjet3d_limite_en_z_etu(t_objet3d* o, t_objet3d* o2, t_objet3d *camera)
* \brief	On compose un objet à un deuxième objet et on n'affiche pas les faces hors du champ de la caméra
* \details	Le second objet ajouté au premier objet ne sera plus utilisable. L'objet composé prend la place du premier objet.
* \param	o un objet
* \param	o un second objet
* \param	camera un objet camera
* \return	void
*/

void composerObjet3d_limite_en_z_etu(t_objet3d* o, t_objet3d* o2, t_objet3d *camera)
{
	//on assemble les deux objets
	composerObjet3d_etu(o, o2);

	//maillon de stockage des maillons du premier objet
	t_maillon *mp = o->tete;
	//stockage maillon précédent
	t_maillon *mpp = o->tete;
	t_bool tete = true;

	//tant que qu'il existe des maillons
	while(mp->pt_suiv != NULL)
	{
		//on calcule la coordonnée z moyenne du maillon/face
		double zmoy = zmoyen(mp->face);
		//si cette coordonnée est dans la zone de la caméra on l'ajoute au maillon de stockage
		if(zmoy < camera->proche || zmoy > camera->loin)
		{ 
			if(tete)
				{ o->tete = mp->pt_suiv; }	
			else
				{ mpp->pt_suiv = mp->pt_suiv; }
		}
		else
		{
			if(tete)
				{ tete = false;}
			mpp = mp;
		}
		//on passe au maillon suivant
		mp = mp->pt_suiv;
	}
};
/**
* \fn		void libererObjet3d_etu(t_objet3d *o)
* \brief	Supprime l Objet 3D
* \details	Toutes les mémoires utilisées par l'objets sont libérées
* \param	o l objet a supprimer
* \return	void
*/

void libererObjet3d_etu(t_objet3d *o)
{
	// on se place sur le 1er maillon (maillon temporaire)
	t_maillon *temp;
	temp = o->tete;
	while(temp != NULL)
	{
		// on libere les points composants une face
		free((temp->face)->abc[0]);
		free((temp->face)->abc[1]);
		free((temp->face)->abc[2]);
		// on libere la face
		free(temp->face);
		// on passe au maillon suivant
		temp = temp->pt_suiv;
	}
	// on libere le maillon temporaire
	free(temp);
	// on libere le 1er maillon de l objet
	free(o->tete);
	// on libere l objet
	free(o);
};

/**
* \fn		t_point3d *centreGraviteObjet3d_etu(t_objet3d *o)
* \brief	Calculer le centre de gravite d un Objet 3D.
* \details	On fait appel a centreGraviteTriangle3d_etu() pour traiter chaque face.
* \param	t un t_point3d
* \return	un t_point3d
*/
t_point3d *centreGraviteObjet3d_etu(t_objet3d *o)
{
	//compteur de faces traitées
	int nb_face = 0;
	int i;
	//point de gravité
	t_point3d *p_grv;
	//point de gravité temporaire
	t_point3d *p_grv_temp;
	//maillon temporaire pour parcourir les maillons
	t_maillon *temp = o->tete;
	while(temp != NULL)
	{	
		//calcul du point de gravité de la face/maillon
		p_grv_temp = centreGraviteTriangle3d_etu(temp->face);
		//copie du point dans le point temporaire
		for (i=0; i<3; i++)
		{
			p_grv->xyzt[i] = p_grv_temp->xyzt[i];
		}
		//on passe au maillon suivant
		temp = temp->pt_suiv;
		//on incrémente le compteur de faces
		nb_face++;
	}
	//on calcule le point de gravité moyen pour toutes les faces
	for (i=0; i<3; i++)
	{
		p_grv->xyzt[i]/nb_face;
	}
	//on libere le point temporaire
	free(p_grv_temp);
	return p_grv;
};

/**
* \fn		t_maillon * merge(t_maillon *m1, t_maillon * m2)
* \brief	Fonction permettant de reformer la liste des maillons d'un objet à partir de deux listes
* \details	
* \param	l1 la première partie de cette liste
* \param	l2 la deuxième partie de cette liste
* \return	un t_maillon 
*/
t_maillon * fusion(t_maillon *l1, t_maillon * l2)
{
	t_maillon *lfusion = NULL;

	if(l1 == NULL)
	{
		lfusion = l2;
	}
	else if(l2 == NULL)
	{
		lfusion = l1;
	}
	//Si aucun des deux points n'est null
	else
	{
		//verifier l'ordre des données
		if(zmoyen_etu(l1->face) <= zmoyen_etu(l2->face))
		{
			lfusion = l1;
			lfusion->pt_suiv = fusion(l1->pt_suiv,l2);
		}
		else
		{
			lfusion = l2;
			lfusion->pt_suiv = fusion(l1,l2->pt_suiv);
		}
	}
	return lfusion;
}

/**
* \fn		void split(t_maillon *source, t_maillon **part1, t_maillon **part2)
* \brief	Fonction permettant de séparer en deux la liste des maillons d'un objet
* \details	
* \param	source la liste à trier
* \param	part1 la première partie de cette liste
* \param	part2 la deuxième partie de cette liste
* \return	void
*/
void split(t_maillon *source, t_maillon **part1, t_maillon **part2)
{
	t_maillon *m = source;
	//compteur pour connaitre la taille de la liste
	int taille = 0;
	int i;

	//si la liste est vide ou s'il n'y a plus de maillon
	if(source == NULL || source->pt_suiv == NULL)
	{
			 *part1 = source;
			 *part2 = NULL;
	}

	else
	{
		while(m != NULL)
		{
			taille++;
			m = m->pt_suiv;
		}
		//premier élément
		m = source; 
		
		for(i = 1; i<taille/2; i++)
			{ m = m->pt_suiv; }

		//le premier élément de la liste = premier élément de la sous liste
		*part1 = source;
		//le premier élément de la deuxième sous liste = l'élément du milieu de la liste
		*part2 = m->pt_suiv;
		m->pt_suiv = NULL; 
	}
}

/**
* \fn		void trierObjet3d(t_maillon **objet)
* \brief	Fonction permettant de trier un objet
* \details	
* \param	surface une surface
* \return	void
*/
void trierObjet3d(t_maillon **objet)
{
	t_maillon * maillon = *objet;
	t_maillon * part1;
	t_maillon * part2;

	//si le premier maillon de l'objet n'est pas nul
	if(maillon != NULL)
	{
		//si l'on n'est pas sur la dernière face de l'objet
		if(maillon->pt_suiv != NULL)
		{
			//on coupe la liste en deux
			split(maillon,&part1,&part2); 
			//trie des deux listes de manières récursives
			trierObjet3d(&part1);
			trierObjet3d(&part2);
			//on fusionne les deux listes triées
			*objet = fusion(part1,part2);
		}
	}
}

/**
* \fn		void dessinerObjet3d_etu(t_surface *surface, t_objet3d* pt_objet, t_objet3d *camera)
* \brief	Colorie un objet3d
* \details	On fait appel a remplirTriangle3d_etu() pour traiter chaque face.
* \param	surface une surface
* \param	pt_objet l'objet à colorier
* \param	camera un objet camera
* \return	void
*/
void dessinerObjet3d_etu(t_surface *surface, t_objet3d* pt_objet, t_objet3d *camera)
{
	//on se place dans le premier maillon
	t_maillon *m = pt_objet->tete;

	//on parcourt les maillons
	if(m != NULL)
	{
		// si le maillon suivant n'est pas nul on dessine la face
		if(!pt_objet->est_trie)
		{
			//fonction annexe permettant de trier les faces sinon il y a des problèmes d'affichage
			trierObjet3d(&m);
		}
	}
	while(m->pt_suiv != NULL)
	{
		// remplirTriangle3d_etu
		remplirTriangle3d(surface, m->face, m->couleur, camera->largeur, camera->hauteur, camera->distance_ecran);
		// on passe au maillon suivant
		m = m->pt_suiv;
	}
};

/**
* \fn		void translationObjet3d_etu(t_objet3d* pt_objet, t_point3d *vecteur)
* \brief	Appliquer une translation a un Objet 3D.
* \details	On ajoute a chaque coordonnees des Points de l Objet celle du vecteur.
* \param	pt_objet un t_objet3d a deplacer
* \param	vecteur un t_triangle3d de translation
* \return	void
*/

void translationObjet3d_etu(t_objet3d* pt_objet, t_point3d *vecteur)
{
	//on se place sur le permier maillon
	t_maillon* maillon = pt_objet->tete;

	//on parcourt les maillons
	while (maillon!=NULL)
	{
		//on applique la translation sur chaque face
		translationTriangle3d_etu(maillon->face, vecteur);
		//on passe au maillon suivant
		maillon = maillon->pt_suiv;
	}
};
void translationObjet3d_fast_etu(t_objet3d* pt_objet, t_point3d *vecteur){};

/**
* \fn		void rotationObjet3d_etu(t_objet3d* pt_objet, t_point3d *centre, float degreX, float degreY, float degreZ)
* \brief	Appliquer une rotation à un Objet 3D.
* \details	On applique la rotation à chaque face (triangle3d) de l'objet.
* \param	pt_objet un t_objet3d a tourner
* \param 	centre de l'objet representant le Point de rotation
* \param	degreX un reel representant l angle de rotation en X
* \param	degreY un reel representant l angle de rotation en Y
* \param	degreZ un reel representant l angle de rotation en Z
* \return	void
*/

void rotationObjet3d_etu(t_objet3d* pt_objet, t_point3d *centre, float degreX, float degreY, float degreZ)
{
	//on se place sur le permier maillon
	t_maillon* maillon = pt_objet->tete;

	//on parcourt les maillons
	while (maillon!=NULL)
	{
		//on applique la rotation sur chaque face
		rotationTriangle3d_etu(maillon->face, centre, degreX, degreY, degreZ);
		//on passe au maillon suivant
		maillon = maillon->pt_suiv;
	}
};
void rotationObjet3d_fast_etu(t_objet3d* pt_objet, t_point3d *centre, float degreX, float degreY, float degreZ){};


void transformationObjet3d_etu(t_objet3d* pt_objet, double mat[4][4])
{
	t_maillon* o = NULL;

	o = pt_objet->tete;
	while (o!=NULL)
	{
		transformationTriangle3d(o->face, mat);
		o = o->pt_suiv;
	}
	pt_objet->est_trie = false;
}